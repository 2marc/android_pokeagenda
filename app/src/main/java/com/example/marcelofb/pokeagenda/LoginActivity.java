package com.example.marcelofb.pokeagenda;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.marcelofb.pokeagenda.async.AsyncConcluidaInterface;
import com.example.marcelofb.pokeagenda.async.AsyncRequest;
import com.example.marcelofb.pokeagenda.util.JsonParseUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by marcelofb on 6/16/18.
 */

public class LoginActivity extends AppCompatActivity implements AsyncConcluidaInterface {
    private String jsonResponse;
    private Treinador treinador;

    EditText usuario;
    EditText senha;
    Button botaoRealizarLogin;

    Treinador t1,t2,t3,t4;

    static ArrayList<Treinador> listaDeTreinadores = new ArrayList<Treinador>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        usuario = (EditText) findViewById(R.id.login_usuario);
        senha = (EditText) findViewById(R.id.login_senha);
        botaoRealizarLogin = (Button) findViewById(R.id.login_botao);

        t1 = new Treinador("Alessandro Brawerman", "alessandro.brawerman", "333333", "333333");
        t2 = new Treinador("Lucas Praca", "lucas.praca", "222222", "222222");
        t3 = new Treinador("Marcelo Brisolla", "marcelo.brisolla", "111111", "111111");
        t4 = new Treinador("Marco Andrey", "marco.andrey", "242424", "242424");

        listaDeTreinadores.add(t1);
        listaDeTreinadores.add(t2);
        listaDeTreinadores.add(t3);
        listaDeTreinadores.add(t4);
    }

    public void realizarLogin(View view) {

        String usuarioInserido = usuario.getText().toString();
        String senhaInserida = senha.getText().toString();

        Treinador treinador = new Treinador();
        treinador.setLogin(usuarioInserido);
        treinador.setSenha(senhaInserida);
        JsonParseUtil jaspion = new JsonParseUtil();

        RequestParams requestParams = new RequestParams();
        requestParams.setEndpoint("http://10.0.2.2:8080/and-poke-agenda-ws/pokedex/treinador/login");
        requestParams.setMetodo(RequestParams.POST);
        requestParams.setOrigem("/treinador/login");
        requestParams.setJson(jaspion.parseLoginToString(treinador));

        AsyncRequest asyncRequest = new AsyncRequest(this, this);
        asyncRequest.execute(requestParams);

    }

    @Override
    public void redirectIntent() {
        Intent intent = new Intent(getApplicationContext(), MenuPrincipalActivity.class);
        if (intent != null) {
            intent.putExtra("treinadorLogado", treinador);
            startActivity(intent);
            this.finish();
        }

    }

    @Override
    public void validateMessage() {
        SharedPreferences sharedPref = getSharedPreferences("Auth",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        JsonParseUtil jaspion = new JsonParseUtil();

        if (!jsonResponse.isEmpty()) {
            if (jaspion.isMensagem(jsonResponse)) {
                Toast.makeText(this, jaspion.parseMensagemFromString(jsonResponse), Toast.LENGTH_SHORT).show();
                usuario.setText("");
                senha.setText("");
            } else {
                System.out.println(jaspion.parseAuthFromString(jsonResponse));
                editor.putString("chave_auth", jaspion.parseAuthFromString(jsonResponse));
                editor.commit();
                treinador = jaspion.parseTreinadorFromString(jsonResponse);
                this.redirectIntent();
            }
        }
    }

    @Override
    public void onAsyncConcluida(String json) {
        this.jsonResponse = json;

        this.validateMessage();
    }
}
