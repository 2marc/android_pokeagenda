package com.example.marcelofb.pokeagenda;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.marcelofb.pokeagenda.async.AsyncConcluidaInterface;
import com.example.marcelofb.pokeagenda.async.AsyncRequest;
import com.example.marcelofb.pokeagenda.util.JsonParseUtil;

import java.util.ArrayList;

/**
 * Created by marcelofb on 6/16/18.
 */

public class MenuPrincipalActivity  extends AppCompatActivity implements AsyncConcluidaInterface {
    private String jsonResponse;
    static ArrayList<Pokemon> listaDePokemons = new ArrayList<Pokemon>();
    static Treinador treinadorLogado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_principal);

        SharedPreferences sharedPref = getSharedPreferences("Auth", Context.MODE_PRIVATE);
        RequestParams requestParams = new RequestParams();
        requestParams.setEndpoint("http://10.0.2.2:8080/and-poke-agenda-ws/pokedex/treinador");
        requestParams.setMetodo(RequestParams.GET);
        requestParams.setOrigem("/pokedex");
        requestParams.setAuthKey(sharedPref.getString("chave_auth",""));

        AsyncRequest asyncRequest = new AsyncRequest(this, this);
        asyncRequest.execute(requestParams);

//        Bundle extras = getIntent().getExtras();
//        if (extras != null) {
//            treinadorLogado = (Treinador) getIntent().getSerializableExtra("treinadorLogado");
//        }



//        MenuPrincipalActivity.listaDePokemons.add(p1);
//        MenuPrincipalActivity.listaDePokemons.add(p2);
//        MenuPrincipalActivity.listaDePokemons.add(p3);
//        MenuPrincipalActivity.listaDePokemons.add(p4);
//        MenuPrincipalActivity.listaDePokemons.add(p5);
    }






    @Override
    public void onAsyncConcluida(String json) {
        this.jsonResponse = json;

        System.out.println(jsonResponse);
        this.validateMessage();
    }

    @Override
    public void validateMessage() {
        JsonParseUtil jaspion = new JsonParseUtil();

        if (!jsonResponse.isEmpty()) {
            if (jaspion.isMensagem(jsonResponse)) {
                Toast.makeText(this, jaspion.parseMensagemFromString(jsonResponse), Toast.LENGTH_SHORT).show();
            } else {
                treinadorLogado = jaspion.parseTreinadorFromString(jsonResponse);
                TextView nomeTreinadorLogado = (TextView) findViewById(R.id.menu_principal_nome_treinador_logado);
                nomeTreinadorLogado.setText(treinadorLogado.getNome());
                if (treinadorLogado.getFavorito() != null) {
                    ImageView imagemPokemonFavorito = (ImageView) findViewById(R.id.menu_principal_imagem_pokemon_favorito);


                    Bitmap imagemEmBitmap = UtilsBase64.decodeBase64(treinadorLogado.getFavorito().getImagemPokemon());
                    imagemPokemonFavorito.setImageBitmap(imagemEmBitmap);
                    //imagemPokemonFavorito.setImageResource(getApplicationContext().getResources().getIdentifier(treinadorLogado.getFavorito().getImagemPokemon(), "drawable", getApplicationContext().getPackageName()));
                }
                this.redirectIntent();
            }
        }
    }

    @Override
    public void redirectIntent() {

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);

        return true;
    }

    @Override
    public void onResume()
    {  // After a pause OR at startup
        super.onResume();
        SharedPreferences sharedPref = getSharedPreferences("Auth", Context.MODE_PRIVATE);
        RequestParams requestParams = new RequestParams();
        requestParams.setEndpoint("http://10.0.2.2:8080/and-poke-agenda-ws/pokedex/treinador");
        requestParams.setMetodo(RequestParams.GET);
        requestParams.setOrigem("/pokedex");
        requestParams.setAuthKey(sharedPref.getString("chave_auth",""));

        AsyncRequest asyncRequest = new AsyncRequest(this, this);
        asyncRequest.execute(requestParams);
    }

//    @Override
//    public void onBackPressed() {
//        new AlertDialog.Builder(this)
//                .setIcon(android.R.drawable.ic_dialog_alert)
//                .setTitle("Log-Out")
//                .setMessage("Tem certeza que deseja realizar o log-out e retornar para a página de log-in?")
//                .setPositiveButton("Sim", new DialogInterface.OnClickListener()
//                {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        Intent intentLogin = new Intent(getApplicationContext(), LoginActivity.class);
//                        if (intentLogin != null)
//                            startActivity(intentLogin);
//                        // MATAR SESSÃO
//                    }
//
//                })
//                .setNegativeButton("Não", null)
//                .show();
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = null;

        switch (item.getItemId()) {
            case R.id.menu_principal_cadastrar_pokemon:
                intent = new Intent(this, CadastrarPokemonActivity.class);
                break;
            case R.id.menu_principal_consultar_pokemons:
                intent = new Intent(this, ConsultarPokemonsActivity.class);
                break;
            case R.id.menu_principal_pesquisar_pokemon:
                intent = new Intent(this, PesquisarPokemonActivity.class);
                break;
            case R.id.menu_principal_logout:
                new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Log-Out")
                        .setMessage("Tem certeza que deseja realizar o log-out e retornar para a página de log-in?")
                        .setPositiveButton("Sim", new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intentLogin = new Intent(getApplicationContext(), LoginActivity.class);
                                if (intentLogin != null){
                                    SharedPreferences sharedPref = getSharedPreferences("Auth", Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = sharedPref.edit();
                                    editor.putString("chave_auth", "");
                                    editor.commit();

                                    startActivity(intentLogin);
                                }

                            }

                        })
                        .setNegativeButton("Não", null)
                        .show();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }

        if (intent != null)
            startActivity(intent);

        return true;
    }
}
