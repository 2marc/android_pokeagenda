package com.example.marcelofb.pokeagenda;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.marcelofb.pokeagenda.async.AsyncConcluidaInterface;
import com.example.marcelofb.pokeagenda.async.AsyncRequest;
import com.example.marcelofb.pokeagenda.util.JsonParseUtil;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by marcelofb on 6/16/18.
 */

public class ConsultarPokemonsActivity extends AppCompatActivity implements AsyncConcluidaInterface{
    private String jsonResponse;

    ListView list;
    ArrayList<Pokemon> listaDePokemons = new ArrayList<Pokemon>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consultar_pokemons);

        SharedPreferences sharedPref = getSharedPreferences("Auth", Context.MODE_PRIVATE);
        RequestParams requestParams = new RequestParams();
        requestParams.setEndpoint("http://10.0.2.2:8080/and-poke-agenda-ws/pokedex/pokemano");
        requestParams.setMetodo(RequestParams.GET);
        requestParams.setOrigem("/pokemano");
        requestParams.setAuthKey(sharedPref.getString("chave_auth",""));

        AsyncRequest asyncRequest = new AsyncRequest(this, this);
        asyncRequest.execute(requestParams);

    }

    @Override
    public void onResume()
    {  // After a pause OR at startup
        super.onResume();
        SharedPreferences sharedPref = getSharedPreferences("Auth", Context.MODE_PRIVATE);
        RequestParams requestParams = new RequestParams();
        requestParams.setEndpoint("http://10.0.2.2:8080/and-poke-agenda-ws/pokedex/pokemano");
        requestParams.setMetodo(RequestParams.GET);
        requestParams.setOrigem("/pokemano");
        requestParams.setAuthKey(sharedPref.getString("chave_auth",""));

        AsyncRequest asyncRequest = new AsyncRequest(this, this);
        asyncRequest.execute(requestParams);
    }

    @Override
    public void onAsyncConcluida(String json) {
        this.jsonResponse = json;

        System.out.println(jsonResponse);
        this.validateMessage();
    }

    @Override
    public void validateMessage() {
        JsonParseUtil jaspion = new JsonParseUtil();

        if (!jsonResponse.isEmpty()) {
            if (jaspion.isMensagem(jsonResponse)) {
                Toast.makeText(this, jaspion.parseMensagemFromString(jsonResponse), Toast.LENGTH_SHORT).show();
            } else {
                listaDePokemons = jaspion.parsePokemanosFromString(jsonResponse);

            ListCell adapter = new ListCell(ConsultarPokemonsActivity.this, listaDePokemons);
            list = (ListView) findViewById(R.id.list);
            list.setAdapter(adapter);
            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                Intent intent = new Intent(getApplicationContext(), DetalhesPokemonActivity.class);
                if (intent != null)
                    intent.putExtra("pokemonSelecionado", listaDePokemons.get(arg2));
                    startActivity(intent);
                //Toast.makeText(ConsultarPokemonsActivity.this, "Clicou na " +nome_pokemon[arg2], Toast.LENGTH_SHORT).show();
            }
        });
                this.redirectIntent();
            }
        }
    }

    @Override
    public void redirectIntent() {

    }
}
