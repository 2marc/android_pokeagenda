package com.example.marcelofb.pokeagenda;

import android.Manifest;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.marcelofb.pokeagenda.async.AsyncConcluidaInterface;
import com.example.marcelofb.pokeagenda.async.AsyncRequest;
import com.example.marcelofb.pokeagenda.util.JsonParseUtil;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by marcelofb on 6/16/18.
 */

public class CadastrarPokemonActivity extends AppCompatActivity implements AsyncConcluidaInterface {

    private String jsonResponse;
    private int PICK_IMAGE_REQUEST = 1;
    static final int REQUEST_IMAGE_CAPTURE = 1000;
    private Bitmap mImageBitmap;
    private String mCurrentPhotoPath;
    private ImageView imageView = null;
    private int POKEMON_EXISTENTE = 0;
    private String imagemEmBase64;
    EditText metros;
    EditText centimetros;
    EditText quilos;
    EditText gramas;
    EditText nome;
    EditText especie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastrar_pokemon);

        Button botao_favoritar = (Button) findViewById(R.id.detalhes_botao_favoritar);
    }

    public void salvarCadastro(View view){
        POKEMON_EXISTENTE = 0;
        metros = (EditText) findViewById(R.id.cadastrar_altura_metros);
        centimetros = (EditText) findViewById(R.id.cadastrar_altura_centimetros);
        quilos = (EditText) findViewById(R.id.cadastrar_peso_quilos);
        gramas = (EditText) findViewById(R.id.cadastrar_peso_gramas);
        nome = (EditText) findViewById(R.id.cadastrar_nome);
        especie = (EditText) findViewById(R.id.cadastrar_especie);

        String metrosNovoPokemon = metros.getText().toString();
        String centimetrosNovoPokemon = centimetros.getText().toString();



        String quilosNovoPokemon = quilos.getText().toString();
        String gramasNovoPokemon = gramas.getText().toString();

        String pesoNovoPokemon = "";
        String alturaNovoPokemon = "";

        String nomeNovoPokemon = nome.getText().toString();
        String especieNovoPokemon = especie.getText().toString();


        if ((quilosNovoPokemon.isEmpty()) || (gramasNovoPokemon.isEmpty()) || (metrosNovoPokemon.isEmpty()) || (centimetrosNovoPokemon.isEmpty())){
            Toast.makeText(this, "É necessário preencher todos os campos e anexar uma imagem!", Toast.LENGTH_SHORT).show();
            metros.setText("");
            centimetros.setText("");
            quilos.setText("");
            gramas.setText("");
            nome.setText("");
            especie .setText("");
            imageView.setImageDrawable(null);
        } else {
            alturaNovoPokemon = metrosNovoPokemon + "." + centimetrosNovoPokemon;
            pesoNovoPokemon = quilosNovoPokemon + "." + gramasNovoPokemon;
        }

        if ((!alturaNovoPokemon.isEmpty()) && (!pesoNovoPokemon.isEmpty()) && (!nomeNovoPokemon.isEmpty()) && (!especieNovoPokemon.isEmpty()) && (imageView != null)){


            SharedPreferences sharedPref = getSharedPreferences("Auth", Context.MODE_PRIVATE);
            RequestParams requestParams = new RequestParams();
            requestParams.setEndpoint("http://10.0.2.2:8080/and-poke-agenda-ws/pokedex/pokemano/cadastrar");
            requestParams.setMetodo(RequestParams.POST);
            requestParams.setOrigem("/cadastrar");
            requestParams.setAuthKey(sharedPref.getString("chave_auth",""));


            Pokemon novoPokemon = new Pokemon(nomeNovoPokemon, especieNovoPokemon, alturaNovoPokemon, pesoNovoPokemon, imagemEmBase64, MenuPrincipalActivity.treinadorLogado);
            JsonParseUtil jaspion = new JsonParseUtil();
            requestParams.setJson(jaspion.parsePokemanoCadastrarToString(novoPokemon));
            AsyncRequest asyncRequest = new AsyncRequest(this, this);
            asyncRequest.execute(requestParams);


//            for (int i = 0; i < MenuPrincipalActivity.listaDePokemons.size(); i++) {
//
//                if (nomeNovoPokemon.equals(MenuPrincipalActivity.listaDePokemons.get(i).getNomePokemon())) {
//                    Toast.makeText(this, "Ô malandro! Pokemon já cadastrado pelo treinador " + MenuPrincipalActivity.listaDePokemons.get(i).getTreinador().getNome(), Toast.LENGTH_SHORT).show();
//                    POKEMON_EXISTENTE = 1;
//                    metros.setText("");
//                    centimetros.setText("");
//                    quilos.setText("");
//                    gramas.setText("");
//                    nome.setText("");
//                    especie .setText("");
//                }
//            }
//
//            if (POKEMON_EXISTENTE != 1) {
//                Pokemon novoPokemon = new Pokemon(nomeNovoPokemon, especieNovoPokemon, alturaNovoPokemon, pesoNovoPokemon, imagemEmBase64, MenuPrincipalActivity.treinadorLogado);
//                MenuPrincipalActivity.listaDePokemons.add(novoPokemon);
//                Toast.makeText(this, "Pokémon cadastrado com sucesso!", Toast.LENGTH_SHORT).show();
//                metros.setText("");
//                centimetros.setText("");
//                quilos.setText("");
//                gramas.setText("");
//                nome.setText("");
//                especie .setText("");
//            }

        } else {

            Toast.makeText(this, "É necessário preencher todos os campos e anexar uma imagem!", Toast.LENGTH_SHORT).show();
            metros.setText("");
            centimetros.setText("");
            quilos.setText("");
            gramas.setText("");
            nome.setText("");
            especie .setText("");
            imageView.setImageDrawable(null);
        }


    }

    @Override
    public void onAsyncConcluida(String json) {
        this.jsonResponse = json;

        System.out.println(jsonResponse);
        this.validateMessage();
    }

    @Override
    public void validateMessage() {
        JsonParseUtil jaspion = new JsonParseUtil();

        if (!jsonResponse.isEmpty()) {
            if (jaspion.isMensagem(jsonResponse)) {
                String respostaJSON = jaspion.parseMensagemFromString(jsonResponse);
                if (!respostaJSON.contains("cadastrado por")){
                    Toast.makeText(this, "Pokémon cadastrado com sucesso!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, jaspion.parseMensagemFromString(jsonResponse), Toast.LENGTH_SHORT).show();
                }
            }
            metros.setText("");
            centimetros.setText("");
            quilos.setText("");
            gramas.setText("");
            nome.setText("");
            especie .setText("");
            imageView.setImageDrawable(null);
        }
    }

    @Override
    public void redirectIntent() {

    }




    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        imageView = (ImageView) findViewById(R.id.cadastrar_foto_novo_pokemon);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {

            Uri uri = data.getData();

            try {
                mImageBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                // Log.d(TAG, String.valueOf(bitmap));
                imageView.setImageBitmap(mImageBitmap);
                imagemEmBase64 = UtilsBase64.encodeBase64(mImageBitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }

        } else if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            try {
                mImageBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), Uri.parse(mCurrentPhotoPath));
                imageView.setImageBitmap(mImageBitmap);
                imagemEmBase64 = UtilsBase64.encodeBase64(mImageBitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void procurarImagemGaleria(View view) {

        Intent intent = new Intent();
// Show only images, no videos or anything else
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
// Always show the chooser (if there are multiple options available)
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);

    }

    public void tirarFoto(View view) {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (cameraIntent.resolveActivity(getPackageManager()) != null) {

//            ContentValues values = new ContentValues(1);
//            values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg");
//            Uri fileUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
//            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
//            cameraIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
//            startActivityForResult(cameraIntent, REQUEST_IMAGE_CAPTURE);

            //Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                //Error occurred while creating the File
                Log.i("LOG", "IOException");
            }
            //Continue only if the File was successfully created
            if (photoFile != null) {
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                startActivityForResult(cameraIntent, REQUEST_IMAGE_CAPTURE);
            }
        }

    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  // prefix
                ".jpg",         // suffix
                storageDir      // directory
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        return image;
    }



}
