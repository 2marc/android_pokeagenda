package com.example.marcelofb.pokeagenda.util;

import com.example.marcelofb.pokeagenda.Pokemon;
import com.example.marcelofb.pokeagenda.Treinador;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by marconunes on 19/06/18.
 */

public class JsonParseUtil {
    private JSONObject jsonObject;
    private JSONArray jsonArray;

    public JsonParseUtil() {
        jsonArray = new JSONArray();
        jsonObject = new JSONObject();
    }

    public Treinador parseTreinadorFromString(String json) {
        Pokemon pokemonFavorito = new Pokemon();
        Treinador treinador = new Treinador();
        try {
            jsonObject = new JSONObject(json);

            treinador.setNome(jsonObject.getString("nome_treinador"));
            treinador.setLogin(jsonObject.getString("login_treinador"));

            if (jsonObject.has("pokemano_favorito")) {
                JSONObject JSONPokemon = jsonObject.getJSONObject("pokemano_favorito");

                //Atribui os objetos que estão nas camadas mais altas
                pokemonFavorito.setNomePokemon(JSONPokemon.getString("nome_pokemon"));
                pokemonFavorito.setTipoPokemon(JSONPokemon.getString("especie"));
                pokemonFavorito.setPesoPokemon(JSONPokemon.getString("peso"));
                pokemonFavorito.setAlturaPokemon(JSONPokemon.getString("altura"));
                pokemonFavorito.setImagemPokemon(JSONPokemon.getString("url_imagem"));
                Treinador t = new Treinador(JSONPokemon.getString("treinador"));
                pokemonFavorito.setTreinador(t);

                treinador.setFavorito(pokemonFavorito);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        return treinador;
    }


    public ArrayList<Pokemon> parsePokemanosFromString(String json) {
        try {
            ArrayList<Pokemon> listaDePokemons = new ArrayList<>();
            Pokemon pokemon;

            JSONObject jsonObj = new JSONObject(json);
            JSONArray array = jsonObj.getJSONArray("pokemanos");


            for (int i = 0; i < array.length(); i++) {
                try {
                    //SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                    //Date data;
                    jsonObject = array.getJSONObject(i);

                    pokemon = new Pokemon();

                    //Atribui os objetos que estão nas camadas mais altas
                    pokemon.setNomePokemon(jsonObject.getString("nome_pokemon"));
                    pokemon.setTipoPokemon(jsonObject.getString("especie"));
                    pokemon.setPesoPokemon(jsonObject.getString("peso"));
                    pokemon.setAlturaPokemon(jsonObject.getString("altura"));
                    pokemon.setImagemPokemon(jsonObject.getString("url_imagem"));
                    Treinador t = new Treinador(jsonObject.getString("treinador"));
                    pokemon.setTreinador(t);

                    listaDePokemons.add(pokemon);
                } catch (JSONException e) {
                    e.printStackTrace();
                    return null;
                }
            }
            return listaDePokemons;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Pokemon parsePokemanoFromString(String json) {
        try {

            Pokemon pokemon;
            JSONObject jsonObj = new JSONObject(json);


                try {

                    pokemon = new Pokemon();
                    //Atribui os objetos que estão nas camadas mais altas
                    pokemon.setNomePokemon(jsonObj.getString("nome_pokemon"));
                    pokemon.setTipoPokemon(jsonObj.getString("especie"));
                    pokemon.setPesoPokemon(jsonObj.getString("peso"));
                    pokemon.setAlturaPokemon(jsonObj.getString("altura"));
                    pokemon.setImagemPokemon(jsonObj.getString("url_imagem"));
                    Treinador t = new Treinador(jsonObj.getString("treinador"));
                    pokemon.setTreinador(t);

                } catch (JSONException e) {
                    e.printStackTrace();
                    return null;
                }

            return pokemon;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String parseMensagemFromString(String json) {
        String retornoJson;
        try {
            jsonObject = new JSONObject(json);
            retornoJson = jsonObject.getString("mensagem");
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        return retornoJson;
    }

    public String parseAuthFromString(String json) {
        String retornoJson;
        try {
            jsonObject = new JSONObject(json);
            retornoJson = jsonObject.getString("chave_auth");
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        return retornoJson;
    }

    public boolean isMensagem(String json) {
        try {
            JSONObject jsonObject = new JSONObject(json);

            if (jsonObject.has("mensagem")) {
                return true;
            } else {
                return false;
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    public String parseLoginToString(Treinador treinador) {
        JSONObject loginJSONobj = new JSONObject();
        try {
            loginJSONobj.put("login_treinador", treinador.getLogin());
            loginJSONobj.put("senha", treinador.getSenha());
            return loginJSONobj.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String parsePokemanoFavoritoToString(Pokemon pokemano) {
        return this.parsePokemanoIdToString(pokemano, "pokemano_favorito");
    }

    public String parsePokemanoPesquisarToString(Pokemon pokemano) {
        return this.parsePokemanoIdToString(pokemano, "pokemano_pesquisado");
    }

    public String parsePokemanoCadastrarToString(Pokemon pokemano) {
        JSONObject pokemonJSONobj = new JSONObject();
        try {
            pokemonJSONobj.put("nome_pokemon", pokemano.getNomePokemon());
            pokemonJSONobj.put("especie", pokemano.getTipoPokemon());
            pokemonJSONobj.put("peso", pokemano.getPesoPokemon());
            pokemonJSONobj.put("altura", pokemano.getAlturaPokemon());
            pokemonJSONobj.put("url_imagem", pokemano.getImagemPokemon());

            return pokemonJSONobj.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    private String parsePokemanoIdToString(Pokemon pokemano, String chave) {
        JSONObject pokemonJSONobj = new JSONObject();
        try {
            pokemonJSONobj.put(chave, pokemano.getNomePokemon());
            return pokemonJSONobj.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public JSONObject getJsonObject() {
        return jsonObject;
    }

    public void setJsonObject(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
    }

    public JSONArray getJsonArray() {
        return jsonArray;
    }

    public void setJsonArray(JSONArray jsonArray) {
        this.jsonArray = jsonArray;
    }
}
