package com.example.marcelofb.pokeagenda;

import java.io.Serializable;

/**
 * Created by marcelofb on 6/17/18.
 */

public class Pokemon implements Serializable {
    private String nomePokemon;
    private String tipoPokemon;
    private String alturaPokemon;
    private String pesoPokemon;
    private String imagemPokemon;
    private Treinador treinador;

    public Pokemon() {}

    public Pokemon (String nomePokemon, String tipoPokemon, String alturaPokemon, String pesoPokemon, String imagemPokemon, Treinador treinador) {
        this.nomePokemon = nomePokemon;
        this.tipoPokemon = tipoPokemon;
        this.alturaPokemon = alturaPokemon;
        this.pesoPokemon = pesoPokemon;
        this.imagemPokemon = imagemPokemon;
        this.treinador = treinador;
    }

    public Treinador getTreinador() {
        return treinador;
    }

    public void setTreinador(Treinador treinador) {
        this.treinador = treinador;
    }

    public String getNomePokemon() {
        return nomePokemon;
    }

    public String getTipoPokemon() {
        return tipoPokemon;
    }

    public String getAlturaPokemon() {
        return alturaPokemon;
    }

    public String getPesoPokemon() {
        return pesoPokemon;
    }

    public String getImagemPokemon() {
        return imagemPokemon;
    }

    public void setNomePokemon(String nomePokemon) {
        this.nomePokemon = nomePokemon;
    }

    public void setTipoPokemon(String tipoPokemon) {
        this.tipoPokemon = tipoPokemon;
    }

    public void setAlturaPokemon(String alturaPokemon) {
        this.alturaPokemon = alturaPokemon;
    }

    public void setPesoPokemon(String pesoPokemon) {
        this.pesoPokemon = pesoPokemon;
    }

    public void setImagemPokemon(String imagemPokemon) {
        this.imagemPokemon = imagemPokemon;
    }
}