package com.example.marcelofb.pokeagenda.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

public class ConnectionUtils {

    private String AUTHORIZATIONKEY;
    private URL endpoint;
    private HttpURLConnection conexao;
    private InputStream is;
    private int statusResponse;

    public ConnectionUtils(String AUTHORIZATIONKEY) {
        this(AUTHORIZATIONKEY, "http://localhost:8080/and-poke-agenda-ws/");
    }

    public ConnectionUtils(String AUTHORIZATIONKEY, String endpoint) {
        this.AUTHORIZATIONKEY = AUTHORIZATIONKEY;
        try {
            this.endpoint = new URL(endpoint);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public int getStatusResponse() {
        return statusResponse;
    }

    //Responsavel por carregar o Objeto JSON
    public String getJSONFromAPI(){

        String retorno = "";
        try {
            conexao = (HttpURLConnection) endpoint.openConnection();
            setRequest("GET");
            conexao.connect();

            statusResponse = conexao.getResponseCode();
            if (statusResponse == 200)
                is = conexao.getInputStream();
            else
                is = conexao.getErrorStream();

            retorno = converterInputStreamToString(is);
            is.close();

            conexao.disconnect();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }

        return retorno;
    }

    public String postJsonToAPI(String json) {
        String retorno = "";
        try {
            int codigoResposta;

            conexao = (HttpURLConnection) endpoint.openConnection();

            setRequest("POST", json);

//            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(conexao.getOutputStream());
//            outputStreamWriter.write();
//            outputStreamWriter.flush();
//            outputStreamWriter.close();


            conexao.connect();

            statusResponse = conexao.getResponseCode();
            if (statusResponse == 200)
                is = conexao.getInputStream();
            else
                is = conexao.getErrorStream();

            retorno = converterInputStreamToString(is);
            is.close();

            conexao.disconnect();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }

        return retorno;
    }

    private void setRequest(String method) {
        try {
            conexao.setRequestMethod(method);
            conexao.setReadTimeout(15000);
            if (this.AUTHORIZATIONKEY != null && !(this.AUTHORIZATIONKEY.isEmpty()))
                conexao.setRequestProperty("Authorization", this.AUTHORIZATIONKEY);
            conexao.setConnectTimeout(15000);
        } catch (ProtocolException e) {
            e.printStackTrace();
        }
    }

    private void setRequest(String method, String json) {
        this.setRequest(method);
        conexao.setRequestProperty("Content-type", "application/json");
        conexao.setDoOutput(true);
        try (PrintStream printStream = new PrintStream(conexao.getOutputStream())) {
            printStream.println(json);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String converterInputStreamToString(InputStream is){
        StringBuffer buffer = new StringBuffer();
        try{
            BufferedReader br;
            String linha;

            br = new BufferedReader(new InputStreamReader(is));
            while((linha = br.readLine())!=null){
                buffer.append(linha);
            }

            br.close();
        }catch(IOException e){
            e.printStackTrace();
        }

        return buffer.toString();
    }
}