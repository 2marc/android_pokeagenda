package com.example.marcelofb.pokeagenda;

import java.io.Serializable;

/**
 * Created by marcelofb on 6/17/18.
 */

public class Treinador implements Serializable {

    private String nome;
    private String login;
    private String senha;
    private String API_KEY;
    private Pokemon favorito;

    public Treinador() {}

    public Treinador(String nome, String login, String senha, String API_KEY) {
        this.nome = nome;
        this.login = login;
        this.senha = senha;
        this.API_KEY = API_KEY;
    }

    public Treinador(String nome, String login, String senha, String API_KEY, Pokemon favorito) {
        this.nome = nome;
        this.login = login;
        this.senha = senha;
        this.API_KEY = API_KEY;
        this.favorito = favorito;
    }

    public Treinador(String nome) {
        this.nome = nome;
    }

    public Pokemon getFavorito() {
        return favorito;
    }

    public void setFavorito(Pokemon favorito) {
        this.favorito = favorito;
    }

    public String getNome() { return nome; }

    public String getLogin() { return login; }

    public String getSenha() { return senha; }

    public String getAPI_KEY() { return API_KEY; }

    public void setNome(String nome) { this.nome = nome; }

    public void setLogin(String login) { this.login = login; }

    public void setSenha(String senha) { this.senha = senha; }

    public void setAPI_KEY(String API_KEY) { this.API_KEY = API_KEY; }
}