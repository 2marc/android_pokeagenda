package com.example.marcelofb.pokeagenda.async;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.example.marcelofb.pokeagenda.RequestParams;
import com.example.marcelofb.pokeagenda.util.ConnectionUtils;

/**
 * Created by marconunes on 18/06/18.
 */

public class AsyncRequest extends AsyncTask<RequestParams, Void, String> {
    private ProgressDialog load;
    private Context context;
    private AsyncConcluidaInterface listener;

    public AsyncRequest(Context context, AsyncConcluidaInterface listener) {
        super();
        this.context = context;
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        load = ProgressDialog.show(context, "Aguarde ...", "Recuperando Dados...");
    }

    @Override
    protected String doInBackground(RequestParams... requestParams) {
        ConnectionUtils cu = new ConnectionUtils(requestParams[0].getAuthKey(), requestParams[0].getEndpoint());

        if (requestParams[0].getMetodo().equals("POST"))
            return cu.postJsonToAPI(requestParams[0].getJson());
        else
            return cu.getJSONFromAPI();
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        listener.onAsyncConcluida(s);
        load.dismiss();
    }
}
