package com.example.marcelofb.pokeagenda;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.marcelofb.pokeagenda.async.AsyncConcluidaInterface;
import com.example.marcelofb.pokeagenda.async.AsyncRequest;
import com.example.marcelofb.pokeagenda.util.JsonParseUtil;

/**
 * Created by marcelofb on 6/16/18.
 */

public class PesquisarPokemonActivity extends AppCompatActivity implements AsyncConcluidaInterface {
    private String jsonResponse;
    EditText campo_pesquisa;
    Button botao_pesquisar;
    int POKEMON_ENCONTRADO = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pesquisar_pokemon);

        campo_pesquisa = (EditText) findViewById(R.id.pesquisar_campo_pesquisa);
        botao_pesquisar = (Button) findViewById(R.id.pesquisar_botao_pesquisar);
    }

    @Override
    public void onResume()
    {  // After a pause OR at startup
        super.onResume();
        POKEMON_ENCONTRADO = 0;
    }

    @Override
    public void onAsyncConcluida(String json) {
        this.jsonResponse = json;

        System.out.println(jsonResponse);
        this.validateMessage();
    }

    @Override
    public void validateMessage() {
        JsonParseUtil jaspion = new JsonParseUtil();

        if (!jsonResponse.isEmpty()) {
            if (jaspion.isMensagem(jsonResponse)) {
                Toast.makeText(this, jaspion.parseMensagemFromString(jsonResponse), Toast.LENGTH_SHORT).show();
            } else {
                Pokemon pokemonEncontrado = jaspion.parsePokemanoFromString(jsonResponse);
                Intent intent = new Intent(getApplicationContext(), DetalhesPokemonActivity.class);
                if (intent != null) {
                    intent.putExtra("pokemonSelecionado", pokemonEncontrado);
                    startActivity(intent);
                }
                campo_pesquisa.setText("");
            }
        }
    }

    @Override
    public void redirectIntent() {

    }

    public void realizarPesquisa(View view) {

        SharedPreferences sharedPref = getSharedPreferences("Auth", Context.MODE_PRIVATE);
        RequestParams requestParams = new RequestParams();
        requestParams.setEndpoint("http://10.0.2.2:8080/and-poke-agenda-ws/pokedex/pokemano/pesquisar");
        requestParams.setMetodo(RequestParams.POST);
        requestParams.setOrigem("/pesquisar");
        requestParams.setAuthKey(sharedPref.getString("chave_auth",""));


        String pesquisaAtual = campo_pesquisa.getText().toString();
        if (!pesquisaAtual.isEmpty()){
            Pokemon pokemonParaPesquisa = new Pokemon();
            pokemonParaPesquisa.setNomePokemon(pesquisaAtual);
            JsonParseUtil jaspion = new JsonParseUtil();
            requestParams.setJson(jaspion.parsePokemanoPesquisarToString(pokemonParaPesquisa));
        } else {
            Toast.makeText(this, "Preencher o nome do Pokémon desejado antes de pesquisar!", Toast.LENGTH_SHORT).show();
        }

        AsyncRequest asyncRequest = new AsyncRequest(this, this);
        asyncRequest.execute(requestParams);

    }

}
