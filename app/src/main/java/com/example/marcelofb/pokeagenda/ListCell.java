package com.example.marcelofb.pokeagenda;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by marcelofb on 6/16/18.
 */

public class ListCell extends ArrayAdapter<Pokemon>{
    private final Activity context;
    private List<Pokemon> listaDePokemons = new ArrayList<>();

    public ListCell(Activity context, ArrayList<Pokemon> listaDePokemons) {
        super(context, 0, listaDePokemons);
        this.context = context;
        this.listaDePokemons = listaDePokemons;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent){
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.list_cell, null, true);
        TextView lista_nome_pokemon = (TextView)rowView.findViewById(R.id.lista_nome_pokemon);
        TextView lista_tipo_pokemon = (TextView)rowView.findViewById(R.id.lista_tipo_pokemon);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.lista_foto_pokemon);
        lista_nome_pokemon.setText(listaDePokemons.get(position).getNomePokemon());
        lista_tipo_pokemon.setText(listaDePokemons.get(position).getTipoPokemon());

        Bitmap imagemEmBitmap = UtilsBase64.decodeBase64(listaDePokemons.get(position).getImagemPokemon());
        imageView.setImageBitmap(imagemEmBitmap);
        return rowView;
    }

}
