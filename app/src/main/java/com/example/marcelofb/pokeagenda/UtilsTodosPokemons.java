//package com.example.marcelofb.pokeagenda;
//
//import android.util.Log;
//
//import com.example.marcelofb.pokeagenda.util.ConnectionUtils;
//
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.util.ArrayList;
//
//public class UtilsTodosPokemons {
//
//    public ArrayList<Pokemon> getInformacao(String end) {
//        String json;
//        ArrayList<Pokemon> retorno;
//        json = ConnectionUtils.getJSONFromAPI(end);
//        Log.i("Resultado", json);
//        retorno = parseJson(json);
//        return retorno;
//    }
//
//    private ArrayList<Pokemon> parseJson(String json) {
//
//        try {
//            ArrayList<Pokemon> listaDePokemons = new ArrayList<Pokemon>();
//            Pokemon pokemon;
//
//            JSONObject jsonObj = new JSONObject(json);
//            JSONArray array = jsonObj.getJSONArray("pokemanos");
//
//
//            for (int i = 0; i < array.length(); i++) {
//                try {
//                    //SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
//                    //Date data;
//                    JSONObject objArray = array.getJSONObject(i);
//
//                    pokemon = new Pokemon();
//
//                    //Atribui os objetos que estão nas camadas mais altas
//                    pokemon.setNomePokemon(objArray.getString("nome_pokemon"));
//                    pokemon.setTipoPokemon(objArray.getString("especie"));
//                    pokemon.setPesoPokemon(objArray.getString("peso"));
//                    pokemon.setAlturaPokemon(objArray.getString("altura"));
//                    pokemon.setImagemPokemon(objArray.getString("url_imagem"));
//                    Treinador t = new Treinador(objArray.getString("treinador"));
//                    pokemon.setTreinador(t);
//
//                    //Imagem eh um objeto
//                    //JSONObject foto = obj.getJSONObject("picture");
//                    //pokemon.setFoto(baixarImagem(foto.getString("large")));
//
//                    listaDePokemons.add(pokemon);
//
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                    return null;
//                }
//            }
//            return listaDePokemons;
//        } catch (JSONException e) {
//            e.printStackTrace();
//            return null;
//        }
//    }
//}
//
////    private Bitmap baixarImagem(String url) {
////        try{
////            URL endereco;
////            InputStream inputStream;
////            Bitmap imagem; endereco = new URL(url);
////            inputStream = endereco.openStream();
////            imagem = BitmapFactory.decodeStream(inputStream);
////            inputStream.close();
////            return imagem;
////        }catch (IOException e) {
////            e.printStackTrace();
////            return null;
////        }
////    }