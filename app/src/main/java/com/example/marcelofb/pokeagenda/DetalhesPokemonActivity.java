package com.example.marcelofb.pokeagenda;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.marcelofb.pokeagenda.async.AsyncConcluidaInterface;
import com.example.marcelofb.pokeagenda.async.AsyncRequest;
import com.example.marcelofb.pokeagenda.util.JsonParseUtil;

/**
 * Created by marcelofb on 6/16/18.
 */

public class DetalhesPokemonActivity extends AppCompatActivity implements AsyncConcluidaInterface {

    TextView altura;
    TextView peso;
    TextView nome;
    TextView especie;
    TextView treinador;
    ImageView foto;
    Button botao_favoritar;
    Pokemon pokemonSelecionado;
    private String jsonResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhes_pokemon);

        altura = (TextView) findViewById(R.id.detalhes_altura);
        peso = (TextView) findViewById(R.id.detalhes_peso);
        nome = (TextView) findViewById(R.id.detalhes_nome);
        especie = (TextView) findViewById(R.id.detalhes_especie);
        treinador = (TextView) findViewById(R.id.detalhes_treinador);
        foto = (ImageView) findViewById(R.id.detalhes_foto);

        botao_favoritar = (Button) findViewById(R.id.detalhes_botao_favoritar);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            pokemonSelecionado = (Pokemon) getIntent().getSerializableExtra("pokemonSelecionado");
            altura.setText(pokemonSelecionado.getAlturaPokemon());
            peso.setText(pokemonSelecionado.getPesoPokemon());
            nome.setText(pokemonSelecionado.getNomePokemon());
            especie.setText(pokemonSelecionado.getTipoPokemon());
            treinador.setText(pokemonSelecionado.getTreinador().getNome());
            Bitmap imagemEmBitmap = UtilsBase64.decodeBase64(pokemonSelecionado.getImagemPokemon());
            foto.setImageBitmap(imagemEmBitmap);
        }

    }

    public void favoritar(View view){

        SharedPreferences sharedPref = getSharedPreferences("Auth", Context.MODE_PRIVATE);
        RequestParams requestParams = new RequestParams();
        requestParams.setEndpoint("http://10.0.2.2:8080/and-poke-agenda-ws/pokedex/treinador/favorito");
        requestParams.setMetodo(RequestParams.POST);
        requestParams.setOrigem("/favorito");
        requestParams.setAuthKey(sharedPref.getString("chave_auth",""));
        JsonParseUtil jaspion = new JsonParseUtil();
        requestParams.setJson(jaspion.parsePokemanoFavoritoToString(pokemonSelecionado));

        AsyncRequest asyncRequest = new AsyncRequest(this, this);
        asyncRequest.execute(requestParams);

    }


    @Override
    public void onResume()
    {  // After a pause OR at startup
        super.onResume();

    }

    @Override
    public void onAsyncConcluida(String json) {
        this.jsonResponse = json;

        System.out.println(jsonResponse);
        this.validateMessage();
    }

    @Override
    public void validateMessage() {

        JsonParseUtil jaspion = new JsonParseUtil();

        if (!jsonResponse.isEmpty()) {
                String jsonString = jaspion.parseMensagemFromString(jsonResponse);
                if (jsonString.contains("favorito")){
                    Toast.makeText(this, "Seu pokémon favorito agora é o: " + pokemonSelecionado.getNomePokemon(), Toast.LENGTH_SHORT).show();
            } else {
                    Toast.makeText(this, jaspion.parseMensagemFromString(jsonResponse), Toast.LENGTH_SHORT).show();
                this.redirectIntent();
            }
        }
    }

    @Override
    public void redirectIntent() {

    }

}
