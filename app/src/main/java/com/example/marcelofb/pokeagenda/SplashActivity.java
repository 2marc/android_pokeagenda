package com.example.marcelofb.pokeagenda;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.marcelofb.pokeagenda.async.AsyncConcluidaInterface;
import com.example.marcelofb.pokeagenda.async.AsyncRequest;
import com.example.marcelofb.pokeagenda.util.JsonParseUtil;

public class SplashActivity extends AppCompatActivity implements AsyncConcluidaInterface {
    private String jsonResponse;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        JsonParseUtil jaspion = new JsonParseUtil();

        SharedPreferences sharedPref = getSharedPreferences("Auth", Context.MODE_PRIVATE);
        RequestParams requestParams = new RequestParams();
        requestParams.setEndpoint("http://10.0.2.2:8080/and-poke-agenda-ws/pokedex/treinador/auth");
        requestParams.setMetodo(RequestParams.GET);
        requestParams.setOrigem("/treinador/auth");
        requestParams.setAuthKey(sharedPref.getString("chave_auth", ""));

        AsyncRequest asyncRequest = new AsyncRequest(this, this);
        asyncRequest.execute(requestParams);

    }

    @Override
    public void onAsyncConcluida(String json) {
        this.jsonResponse = json;
        this.validateMessage();
    }

    @Override
    public void validateMessage() {
        JsonParseUtil jaspion = new JsonParseUtil();

        if (!jsonResponse.isEmpty()) {
            if (jaspion.isMensagem(jsonResponse)) {
                //Toast.makeText(this, jaspion.parseMensagemFromString(jsonResponse), Toast.LENGTH_SHORT).show();
                if (jaspion.parseMensagemFromString(jsonResponse).contains(" autenticado.")) {
                    intent = new Intent(getApplicationContext(), MenuPrincipalActivity.class);
                } else {
                    intent = new Intent(getApplicationContext(), LoginActivity.class);
                }
                this.redirectIntent();
            }
        }
    }

    @Override
    public void redirectIntent() {
        if (intent != null) {
            startActivity(intent);
            this.finish();
        }
    }
}
