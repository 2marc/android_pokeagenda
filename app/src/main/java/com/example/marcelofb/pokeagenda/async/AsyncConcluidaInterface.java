package com.example.marcelofb.pokeagenda.async;

/**
 * Created by marconunes on 19/06/18.
 */

public interface AsyncConcluidaInterface {
    void onAsyncConcluida(String json);
    void validateMessage();
    void redirectIntent();
}
