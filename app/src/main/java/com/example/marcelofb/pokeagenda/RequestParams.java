package com.example.marcelofb.pokeagenda;

/**
 * Created by marconunes on 18/06/18.
 */

public class RequestParams {
    public final static String POST = "POST";
    public final static String GET = "GET";

    private String origem;
    private String endpoint;
    private String metodo;
    private String json;
    private String authKey;

    public RequestParams() {
        this.origem = "";
        this.authKey = "";
        this.endpoint = "";
        this.metodo = "GET";
        this.json = "";
    }

    public RequestParams(String origem, String endpoint, String metodo, String json, String authKey) {
        this.origem = origem;
        this.endpoint = endpoint;
        this.metodo = metodo;
        this.json = json;
        this.authKey = authKey;
    }

    public String getOrigem() {
        return origem;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setOrigem(String origem) {
        this.origem = origem;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getMetodo() {
        return metodo;
    }

    public void setMetodo(String metodo) {
        this.metodo = metodo;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }

    public String getAuthKey() {
        return authKey;
    }

    public void setAuthKey(String authKey) {
        this.authKey = authKey;
    }
}
